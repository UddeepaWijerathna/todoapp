import { Component, OnInit,Input, EventEmitter, Output  } from '@angular/core';
import { Todo } from 'src/app/todo';
import {ServiceService} from '../../../service.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  @Input() todo:Todo;
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();
  @Output() completeTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoService:ServiceService) { }

  ngOnInit(): void {
  }

  setClasses() {
    let classes = {
      todo: true,
      'is-complete': this.todo.completed
    }

    return classes;
  }

  onDelete(todo){
    this.deleteTodo.emit(todo);
  }

  onComplete(todo){
    this.completeTodo.emit(todo);
  }

}
