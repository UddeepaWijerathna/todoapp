import { Component, OnInit ,EventEmitter,Output} from '@angular/core';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  @Output() addTodo: EventEmitter<any>=new EventEmitter();
  constructor() { }

  title:string;

  ngOnInit(): void {
  }

  onSubmit(form){
    const todo = {
      title:this.title,
      completed: false
    }
    if (form.valid) {
    this.addTodo.emit(todo)
    }
  }

}
