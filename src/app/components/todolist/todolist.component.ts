import { Component, OnInit } from '@angular/core';
import { Todo } from '../../todo';
import {ServiceService} from '../../service.service';


@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {
  
  todos:Todo[];
  todo:Todo;
  
  

  constructor(private todoService:ServiceService) { }

  ngOnInit(): void {
    this.todoService.getTodos().subscribe(todos =>{
      this.todos=todos; 
    });
  }

  deleteTodo(todo:Todo){
    this.todos=this.todos.filter(t=>t.id !== todo.id);
  }

  addTodo(todo:Todo){
      this.todos.push(todo)

  }

  completeTodo(todo:Todo){
    this.todos.find(t=>t.id===todo.id).completed=true;
    this.todoService.completeTodo(todo).subscribe(todo => console.log(todo));
  
  }
  

}
