import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import {Todo} from './todo'
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  todoUrl: string ='../assets/todo.json'; 
  //todoUrl: string ='https://jsonplaceholder.typicode.com/todos';

  constructor(private http:HttpClient) { }

  getTodos(): Observable<Todo[]>{
    return this.http.get<Todo[]>(this.todoUrl);

    
  }

  completeTodo(todo:Todo):Observable<any>{
    const url = `${this.todoUrl}/${todo.id}`;
    return this.http.put(url, todo, httpOptions);

  }
}
